import time
import signal
import json
import csv
import socket

class ParserService:
    def __init__(self):
        signal.signal(signal.SIGINT, self.sigint_handler)   
        self.__running = True

    def isRunning(self):
        return self.__running    

    def stop_service(self):
        print("Service stop!")
        self.__running = False
        exit(0)

    def sigint_handler(self, sig, frame):
        self.stop_service()

    def read_path_from_config(self):
        try:
            with open('config.json') as json_file:
                data = json.load(json_file)
                return data['path'] 
        except EnvironmentError:
            print("Config file not found!")
            self.stop_service()
        except json.decoder.JSONDecodeError:
            print("Config file format invalid, try JSON!")
            self.stop_service()
        except KeyError:
            print("Config file does not contain csv file path!")
            self.stop_service()
    
    def read_data_from_csv(self, path):
        try:
            with open(path, 'r') as csv_data:
                while self.isRunning():
                    csv_reader = csv.reader(csv_data)
                    try:
                        next(csv_reader)
                        list = []
                        for row in csv_reader:
                            element = { "id": int(row[0]), "name": str(row[1]), "value1": float(row[2]), "value2": float(row[3])}
                            list.append(element)
                        if not list:
                            return
                        else:
                            return json.dumps(list)
                    except StopIteration:
                        print("No header found in csv file!")
                        self.stop_service()
        except EnvironmentError:
            print("Data file not found!")
            self.stop_service()

    def run(self):
        try:
            port = int(sys.argv[1])
        except:
            port = 9999
        try:
            host = sys.argv[2]
        except:
            host = 'localhost'

        path = self.read_path_from_config()
        while self.isRunning():
            json_data = self.read_data_from_csv(path)
            if not json_data:
                print("No data found within csv file, checking again in 30 seconds...")
            else:
                print("Port: " + str(port))
                print("Host: " + host)
                print(json_data)
                sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                try:
                    sock.sendto(bytearray(json_data, 'utf-8'), (host, port))
                    response = sock.recvfrom(128*1024)
                    print('Response: {!r}'.format(response))
                finally:
                    sock.close()
            time.sleep(30)
        
service = ParserService()
service.run()